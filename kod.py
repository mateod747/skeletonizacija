import matplotlib
import matplotlib.pyplot as plt
import skimage.io as io
from skimage import filters
from imageio import imread, volread
from skimage import data

original =  io.imread( './slike/ok.png', as_gray=True)

otsu_threshold = filters.threshold_otsu(original)   
binary_image = original < otsu_threshold  

def neighbours(x,y,image):
    img = image
    x_1, y_1, x1, y1 = x-1, y-1, x+1, y+1
    return [ img[x_1][y], img[x_1][y1], img[x][y1], img[x1][y1],     # P2,P3,P4,P5
                img[x1][y], img[x1][y_1], img[x][y_1], img[x_1][y_1] ]    # P6,P7,P8,P9

def transitions(neighbours):
    n = neighbours + neighbours[0:1]      # P2, P3, ... , P8, P9, P2
    return sum( (n1, n2) == (0, 1) for n1, n2 in zip(n, n[1:]) )  # (P2,P3), (P3,P4), ... , (P8,P9), (P9,P2)

def zhangSuen(image):
    thinned_image = image.copy()  # deep kopija da se zaštiti original slika
    changing1 = changing2 = 1       # točke koje će se ukloniti (pikseli)
    while changing1 or changing2:   # iteracija se vrši dok god više nema promjena na slici
        # Korak 1
        changing1 = []
        rows, columns = thinned_image.shape               # x je red, y stupac
        for x in range(1, rows - 1):                     # broj redova
            for y in range(1, columns - 1):           # broj stupaca
                P2,P3,P4,P5,P6,P7,P8,P9 = n = neighbours(x, y, thinned_image)
                if (thinned_image[x][y] == 1     and    # Uvjet 0: P1 je crni piksel s 8 susjeda
                    2 <= sum(n) <= 6   and    # Uvjet 1: 2 <= N(P1) <= 6
                    transitions(n) == 1 and    # Uvjet 2: S(P1)=1  
                    P2 * P4 * P6 == 0  and    # Uvjet 3   
                    P4 * P6 * P8 == 0):         # Uvjet 4
                    changing1.append((x,y))
        for x, y in changing1: 
            thinned_image[x][y] = 0
        # Korak 2
        changing2 = []
        for x in range(1, rows - 1):
            for y in range(1, columns - 1):
                P2,P3,P4,P5,P6,P7,P8,P9 = n = neighbours(x, y, thinned_image)
                if (thinned_image[x][y] == 1   and        # Uvjet 0
                    2 <= sum(n) <= 6  and       # Uvjet 1
                    transitions(n) == 1 and      # Uvjet 2
                    P2 * P4 * P8 == 0 and       # Uvjet 3
                    P2 * P6 * P8 == 0):            # Uvjet 4
                    changing2.append((x,y))    
        for x, y in changing2: 
            thinned_image[x][y] = 0
    return thinned_image

"Primijeni algoritam"
skeleton = zhangSuen(binary_image)

#Rezultati
fig, ax = plt.subplots(1, 2)
ax1, ax2 = ax.ravel()
ax1.imshow(binary_image, cmap=plt.cm.gray)
ax1.set_title('Original binary image')
ax1.axis('off')
ax2.imshow(skeleton, cmap=plt.cm.gray)
ax2.set_title('Skeleton of the image')
ax2.axis('off')
plt.show()


